# swtUI4
基于swt，jface特殊场景还会集成awt，swing等组件封装为一体化的java桌面应用程序框架，定制自己的界面美化特效，定制自己的升级策略，</br>
当内部集成jvm，还可以提供跨平台无jdk的运行程序，希望有兴趣的可以一起维护和扩展，扩展rcp，swt方向在国内市场的应用。 </br>


联系人：小伟     qq:2642000280   如果您有同样的需求和更好的想法，请和我去的沟通哦，谢谢。</br>

启动主界面：</br>
com.ztesoft.ip.ui.MainApp</br>
</br>
  ![image](http://static.oschina.net/uploads/space/2015/0626/174539_8cGZ_1458314.png)
  ![image](http://static.oschina.net/uploads/space/2015/0626/174547_AcJj_1458314.png)
  ![image](http://static.oschina.net/uploads/space/2015/0626/174554_jdHJ_1458314.png)
 
  </br>
发布升级日志</br>
v1.0 2015年6月18日 13:50:38</br>
1.初始化项目 </br>
2.业务背景：切换到不同的网络环境设置繁琐的ip dns很麻烦，用跨平台swt来做，界面可以美化，支持用户自定义，目前只支持颜色</br>

v2.0 2015年6月23日 17:42:10</br>
1.改造为maven工程</br>
2.实现更新机制</br>
3.自动修改ip dns  自动获取功能已经正常使用。</br>